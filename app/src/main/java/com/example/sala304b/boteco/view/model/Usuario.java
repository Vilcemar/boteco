package com.example.sala304b.boteco.view.model;

import java.io.Serializable;

/**
 * Created by sala304b on 08/05/2018.
 */

public class Usuario implements Serializable {

    private int id;
    private String nome;
    private String email;
    private String celular;
    private String senha;
    private String ConfSenha;



    public Usuario(){

    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getConfSenha() {
        return ConfSenha;
    }

    public void setConfSenha(String confSenha) {
        ConfSenha = confSenha;
    }

    @Override
    public String toString(){
        return this.nome;
    }
}
