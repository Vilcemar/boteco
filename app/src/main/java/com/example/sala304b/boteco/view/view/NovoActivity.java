package com.example.sala304b.boteco.view.view;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.sala304b.boteco.R;
import com.example.sala304b.boteco.view.dao.BotecoDAO;
import com.example.sala304b.boteco.view.dao.UsuarioDAO;
import com.example.sala304b.boteco.view.model.Usuario;

import java.util.ArrayList;
import java.util.List;

public class NovoActivity extends AppCompatActivity{

    public static final int REQUEST_IMAGE_CAPTURE = 1;


    private EditText editTextNome;
    private EditText editTextEmail;
    private EditText editTextCelular;
    private EditText editTextSenha;
    private EditText editTextConfSenha;
    private Button buttonCadastrar;
    private Button buttonEntrar;

    private String caminhoArquivo;
    private BotecoDAO dao;
    private Usuario usuario;




    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.usuario);

        editTextNome = findViewById(R.id.nome);
        editTextEmail = findViewById(R.id.email);
        editTextCelular = findViewById(R.id.celular);
        editTextSenha = findViewById(R.id.senha);
        editTextConfSenha = findViewById(R.id.confsenha);
        buttonCadastrar = findViewById(R.id.btncadastrar);
        buttonEntrar = findViewById(R.id.btnentrar);
    }

    private boolean IsPreenchido(String valeu){
        return (valeu!= null && !valeu.isEmpty());


    }

    public void validarDados() throws Exception{
        List<String> listaCamposRequeridos = new ArrayList<>();

        if (!IsPreenchido(usuario.getNome())){
            listaCamposRequeridos.add("Nome");
            editTextNome.setBackgroundColor(getResources().getColor(R.color.preto));
        }
        if (!IsPreenchido(usuario.getEmail())){
            listaCamposRequeridos.add("Email");

        }
        if (!IsPreenchido(usuario.getCelular())){
            listaCamposRequeridos.add("Celular");
        }
        if (!IsPreenchido(usuario.getSenha())){
            listaCamposRequeridos.add("Senha");

        }
        if (!IsPreenchido(usuario.getCelular())){
            listaCamposRequeridos.add("Confirme a Senha");

        }
       if (listaCamposRequeridos.size() >0 ){
            throw new Exception("campos requeridos(s)" + listaCamposRequeridos.toString());
       }

    }
    public void salvar(View view){

        try {
            String nome = editTextNome.getText().toString();
            String email = editTextEmail.getText().toString();
            String celular = editTextCelular.getText().toString();
            String senha = editTextSenha.getText().toString();
            String confsenha = editTextConfSenha.getText().toString();

            usuario = new Usuario();
            usuario.setNome(nome);
            usuario.setEmail(email);
            usuario.setCelular(celular);
            usuario.setSenha(senha);
            usuario.setConfSenha(confsenha);

            this.validarDados();

            dao = new BotecoDAO();
            dao.salvar(usuario);
            dao.close();
            setResult(RESULT_OK);
            finish();

        }catch (Exception ex){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Erro de validação")
                    .setMessage(ex.getMessage());
            AlertDialog dialog = builder.create();
            dialog.show();

        }
    }
}

