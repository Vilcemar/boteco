package com.example.sala304b.boteco.view.model;


import java.io.Serializable;

public class Boteco implements Serializable {

    private int id;
    private String nome_estabelecimento;
    private String nome_proprietario;
    private String nome_prato;
    private String email;
    private String telefone;
    private String cnpj;
    private String cpf;
    private String endereco;
    private String bairro;
    private String cidade;
    private String estado;
    private String site;
    private String horario_abertura;
    private String horario_fechamento;
    private String imagem_logo;
    private String imagem_prato;

    public String getImagem_logo() {
        return imagem_logo;
    }

    public void setImagem_logo(String imagem_logo) {
        this.imagem_logo = imagem_logo;
    }

    public String getImagem_prato() {
        return imagem_prato;
    }

    public void setImagem_prato(String imagem_prato) {
        this.imagem_prato = imagem_prato;
    }

    public Boteco(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome_estabelecimento() {
        return nome_estabelecimento;
    }

    public void setNome_estabelecimento(String nome_estabelecimento) {
        this.nome_estabelecimento = nome_estabelecimento;
    }

    public String getNome_proprietario() {
        return nome_proprietario;
    }

    public void setNome_proprietario(String nome_proprietario) {
        this.nome_proprietario = nome_proprietario;
    }

    public String getNome_prato() {
        return nome_prato;
    }

    public void setNome_prato(String nome_prato) {
        this.nome_prato = nome_prato;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getHorario_abertura() {
        return horario_abertura;
    }

    public void setHorario_abertura(String horario_abertura) {
        this.horario_abertura = horario_abertura;
    }

    public String getHorario_fechamento() {
        return horario_fechamento;
    }
    @Override
   public String toString(){
        return this.nome_estabelecimento;
   }
}
