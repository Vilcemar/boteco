package com.example.sala304b.boteco.view.view;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sala304b.boteco.R;
import com.example.sala304b.boteco.view.model.Boteco;

/**
 * Created by sala304b on 08/05/2018.
 */

public class DetalheActivity extends AppCompatActivity {
    private Boteco boteco;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhe);

        Intent intent = getIntent();
        boteco = (Boteco) intent.getSerializableExtra(MainActivity.BOTECO);

        if (boteco != null) {

            TextView textViewNome_estabelecimento = findViewById(R.id.textTitulo);
            TextView textViewNome_prato = findViewById(R.id.textPrato);
            ImageView imageViewLogo = findViewById(R.id.imagemLogo);
            ImageView imageViewPrato = findViewById(R.id.imagemPrato);

            Bitmap imagemLogo = MainActivity.getImagem();
            Bitmap imagemPrato = MainActivity.getImagem();

        }
    }
}
