package com.example.sala304b.boteco.view.view;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.sala304b.boteco.R;
import com.example.sala304b.boteco.view.model.AdapterBoteco;
import com.example.sala304b.boteco.view.model.Boteco;
import com.example.sala304b.boteco.view.dao.BotecoDAO;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static final int REQUEST_NOVO = 1;

    private static Bitmap imagem;

    public static Bitmap getImagem() {
        return imagem;
    }

    public static final String BOTECO = "boteco";
    public static final String IMAGEM = "imagem";

    private ListView listView;
    private List<Boteco> lista = new ArrayList<>();
    private AdapterBoteco adapter;

    private Boteco botecoselecionado;
    private BotecoDAO dao;

    /** public void atualizarLista(){
     dao = new BotecoDAO(this);
     lista = dao.list();
     dao.close();

     }**/

    protected void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        setContentView(R.layout.activity_main);

        // atualizarLista();

        listView = findViewById(R.id.ListaBotecos);
        adapter = new AdapterBoteco(this, lista);

        listView.setAdapter(adapter);

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                return false;
            }


            public void onItemClick(AdapterView<?> adapter, View contexto, int posicao, long indice) {

                botecoselecionado = (Boteco) adapter.getItemAtPosition(posicao);
                Intent intent = new Intent(MainActivity.this, DetalheActivity.class);
                intent.putExtra(BOTECO, botecoselecionado);
                startActivity(intent);
            }
        });
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapter, View view, int posicao, long indice) {
                botecoselecionado = (Boteco) adapter.getItemAtPosition(posicao);

                return false;
            }
        });

        registerForContextMenu(listView);

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        MenuItem menuItemVisitar = menu.add("Visitar Site");
        menuItemVisitar.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {

                Intent intent = new Intent(Intent.ACTION_VIEW);

                Uri uri = Uri.parse(botecoselecionado.getSite());
                intent.setData(uri);

                startActivity(intent);

                return false;
            }
        });

    /**    MenuItem menuItemLigar = menu.add("Ligar");

        menuItemLigar.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                Intent intent = new Intent(Intent.ACTION_CALL);
                Uri discar = Uri.parse("tel:" + botecoselecionado.getTelefone());
                intent.setData(discar);


                startActivity(intent);

                return false;
            }
        });**/

        menu.add("Enviar Email");
        menu.add("Como chegar");

    }
    public boolean OnCreateOptionsMenu(Menu menu){

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);

        return true;
    }

    public void novo(MenuItem item){
        Intent intent = new Intent(this, NovoActivity.class);
        startActivityForResult(intent, REQUEST_NOVO);
    }

    protected void onResume(){
        super.onResume();
        //atualizarLista();
        adapter.notifyDataSetChanged();


    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode == REQUEST_NOVO){
            switch (requestCode){
                case RESULT_OK:
                    //atualizarLista();
                    adapter.notifyDataSetChanged();
                    break;
                case RESULT_CANCELED:
                    Toast.makeText(this, "cancelou", Toast.LENGTH_LONG).show();
                    break;
            }
        }

    }

    public void sobre (MenuItem item){
        Intent intent = new Intent(this, SobreActivity.class);
        startActivity(intent);
    }
}
